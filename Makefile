CC=gcc -Wall -c

programa: programa.o	
	gcc obj/$< -o bin/$@

programa.o: src/insertion_sort.c
	$(CC) $< -o obj/$@

.PHONY: clean
clean:
	rm bin/*



