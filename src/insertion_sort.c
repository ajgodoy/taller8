#include <stdio.h>
#include <stdlib.h>

typedef int(*comp)(int ,int );

void insertionSort(int arr[],int n, comp c);

void insertionSort(int arr[], int n, comp c)
{
    int i, key, j;
    
    for (i = 1; i < n; i++)
    {
        key = arr[i];
        j = i - 1;
        
        while (j >= 0 && c(arr[j], key) >0)
        {
            
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
    
}

int comp1(int a,int b){
    if(a<b){
        return -1;
    }else if(a>b){
        return 1;
    }else{
        return 0;
    }
}

int comp2(int a,int b){
    if(a<b){
        return 1;
    }else if(a>b){
        return -1;
    }else{
        return 0;
    }
}

int comp_extrano(int a,int b){
     if(a<b){
        return 1;
    }else if(a>b){
        return 0;
    }else{
        return -1;
    }
}
int main(){
	int arreglo1[10]={5,3,9,1,4,2,11,23,8,10};
	int arreglo2[10]={5,3,9,1,4,2,11,23,8,10};
	int arreglo3[10]={5,3,9,1,4,2,11,23,8,10};
	insertionSort(arreglo1,10,&comp1);
	insertionSort(arreglo2,10,&comp2);
	insertionSort(arreglo3,10,&comp_extrano);
	int i,j,k;
    printf("\nArreglo con orden ascendente (comp1)\n");
	for(i=0;i<10;i++){
		printf("%d ",arreglo1[i]);}
	printf("\n");
    printf("\nArreglo con orden descendente (comp2)\n");
	for(j=0;j<10;j++){
		printf("%d ",arreglo2[j]);}
	printf("\n");
    printf("\nArreglo con orden_extraño (comp_extraño)\n");
	for(k=0;k<10;k++){
		printf("%d ",arreglo3[k]);}
	printf("\n\n");
	return 0;

}

